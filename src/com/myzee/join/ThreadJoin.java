
package com.myzee.join;

import java.util.Arrays;
import java.util.List;

public class ThreadJoin {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread t2 = new Thread(new MyThread1());
		
		//passing t2 inside t1, so t2 should start inside t1 thread.
		Thread t1 = new Thread(new MyThread(t2));
		
//		t1.start();
		
		List<Integer> list = Arrays.asList(1,2,1,2,1,2);
//		list.stream().sorted().collect(Collectors.toList()).forEach(System.out::println);;
		
//		int sum = list.stream().reduce(0, (a, b) -> a+b);
		//OR
		int sum = list.stream().reduce(0, Integer::sum);
		
		System.out.println(sum);
		
	}

}

class MyThread implements Runnable {
	
	Thread mt;
	public MyThread(Thread mt) {
		this.mt = mt;
	}
	
	@Override
	public void run() {
		for(int i=0; i < 10; i++) {
			System.out.println("MyThread - " + i);
			if(i == 2)
			try {
				synchronized(mt) {
					mt.start();
					mt.join();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}

class MyThread1 implements Runnable {
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 10; i++) {
			System.out.println("MyThread_1 - " + i);
		}
		
	}
}
