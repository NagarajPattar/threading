/*
 * Not Working
 */

package com.myzee.setpriority;

public class ThreadPriority {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Thread t1 = new Thread(new MThread());
		t1.setName("t1");
		
		Thread t2 = new Thread(new MThread());
		t2.setName("t2");
		
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getPriority());
		
		t1.setPriority(8);
		t2.setPriority(4);
		t1.start();
		t2.start();
	}

}

class MThread implements Runnable{

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getPriority());
	}
	
}
